<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Khoa extends Model
{
    //
    protected $table = 'khoa';

    public function nguoimuon()
    {
        return $this->hasMany('App\NguoiMuon', 'idKhoa', 'id');
    }
}
