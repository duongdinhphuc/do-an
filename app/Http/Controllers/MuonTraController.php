<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MuonTra;
use App\NguoiMuon;
use App\ThietBi;
use App\PhongHoc;
use Carbon\Carbon;
class MuonTraController extends Controller
{
    //
    public function getDsNguoiMuon()
    {
        $nguoimuon = NguoiMuon::all();
        return view('muontra.dsnguoimuon',['nguoimuon'=> $nguoimuon]);
    }

    public function getDanhsach($idNguoiMuon)
    {
        $muontra = MuonTra::where('idNguoiMuon',$idNguoiMuon)->get();
        return view('muontra.danhsach',['muontra'=>$muontra]);
    }

    public function getThongKe()
    {
        $muontra = MuonTra::all();
        return view('muontra.thongke',['muontra'=>$muontra]);
    }
    public function getThem($id)
    {

        $nguoimuon = NguoiMuon::find($id);
        $thietbi = ThietBi::all();
        $phonghoc = PhongHoc::all();
        return view('muontra.them', ['nguoimuon' => $nguoimuon, 'thietbi' => $thietbi, 'phonghoc' => $phonghoc]);
    }

    public function postThem(Request $request, $id)
    {
        $this->validate(
            $request,
            [
                'SoLuongMuon' => 'required'
            ],
            [
                'SoLuongMuon.required' => 'Bạn chưa nhập số lượng mượn'
            ]
        );
        $mt = MuonTra::all();
        //$muontra = MuonTra::find($id);
        $getcurrentdate = Carbon::now()->toDateString();
        // $dk1 = MuonTra::where('idPhong', '=', $request->PhongHoc)->where('TietMuon', '=', $request->TietMuon)->whereDate('created_at', '=', $getcurrentdate)->get();
        // $dk2 = MuonTra::where('idNguoiMuon', '=', '$request->NguoiMuon')->where('TinhTrang', '=', 'Chưa trả')->get();

        $thongbao ="";
        $kt = true;

        foreach ($mt as $m)
         {
            if ($request->PhongHoc == $m->idPhong && $request->TietMuon == $m->TietMuon && date('Y-m-d', strtotime($m->created_at)) == $getcurrentdate && $request->NguoiMuon != $m->idNguoiMuon && $request->ThietBi == $m->idThietBi && $m->TinhTrang == 'Chưa trả' ) {
                $thongbao = 'Phòng này đã có '.$m->NguoiMuon->LoaiNguoiMuon.' - '.$m->NguoiMuon->TenNguoiMuon.' mượn thiết bị và đang học.';
                $kt = false;

            } else if($request->NguoiMuon == $m->idNguoiMuon && $request->ThietBi == $m->idThietBi && $m->TinhTrang == "Chưa trả") {
                $thongbao = 'Người này chưa trả thiết bị '.$m->ThietBi->TenThietBi.' từ lần trước, xin vui lòng trả để mượn tiếp';
                $kt = false;

            }
        }

        if ($kt) {
            $muontra = new MuonTra;
            $muontra->idNguoiMuon = $request->NguoiMuon;
            $muontra->idThietBi = $request->ThietBi;
            $muontra->SoLuongMuon = $request->SoLuongMuon;
            $muontra->idPhong = $request->PhongHoc;
            $muontra->TietMuon = $request->TietMuon;
            $muontra->TinhTrang = $request->TinhTrang;
            $muontra->save();
            $thietbi = ThietBi::findOrFail($request->ThietBi);
            (int)$thietbi->SoLuongDangMuon = (int)$thietbi->SoLuongDangMuon + (int)$request->SoLuongMuon;
            $thietbi->save();
            $thongbao = 'Đã thêm thành công';
        }
        return redirect('muontra/them/' . $id)->with('thongbao', $thongbao);


    }

    public function getSua($id)
    {
        $muontra = MuonTra::find($id);
        $nguoimuon = NguoiMuon::all();
        $thietbi = ThietBi::all();
        $phonghoc = PhongHoc::all();
        return view('muontra.sua', ['muontra' => $muontra, 'nguoimuon' => $nguoimuon, 'thietbi' => $thietbi, 'phonghoc' => $phonghoc]);
    }

    public function postSua($id, Request $request)
    {
        $this->validate(
            $request,
            [
                'SoLuongTra' => 'required',
                'SoLuongHong'=>'required'
            ],
            [
                'SoLuongTra.required' => 'Bạn chưa nhập số lượng trả',
                'SoLuongHong.required' => 'Bạn chưa nhập số lượng hỏng',
            ]
        );

        $thietbi = ThietBi::findOrFail($request->ThietBi);
        (int)$thietbi->SoLuongDangMuon = (int)$thietbi->SoLuongDangMuon - (int)$request->SoLuongTra;
        (int)$thietbi->SoLuongHong = (int)$thietbi->SoLuongHong + (int)$request->SoLuongHong;
        $thietbi->save();


        $muontra = MuonTra::find($id);
        (int)$muontra->SoLuongTra = (int)$muontra->SoLuongTra + (int)$request->SoLuongTra;
        $muontra->TinhTrang = $request->TinhTrang;
        $muontra->save();

        return redirect('muontra/sua/' . $id)->with('thongbao', 'Đã cập nhật thành công');
    }
}
