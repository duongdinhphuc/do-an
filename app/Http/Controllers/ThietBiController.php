<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ThietBi;
class ThietBiController extends Controller
{
    //
    public function getDanhsach()
    {
        $thietbi = ThietBi::all();
        return view('thietbi.danhsach', ['thietbi' => $thietbi]);
    }

    public function getThem()
    {
        return view('thietbi.them');
    }

    public function postThem(Request $request)
    {
        $this->validate(
            $request,
            [
                'TenThietBi' => 'required',
                'TongSoLuong' => 'required|integer',
                'SoLuongHong' => 'required|integer'
            ],
            [
                'TenThietBi.required' => 'Bạn chưa nhập tên thiết bị',
                'TongSoLuong.required' => 'Bạn chưa nhập số lượng thiết bị tốt',
                'TongSoLuong.integer' => 'Số lượng phải là một số nguyên',
                'SoLuongHong.required' => 'Bạn chưa nhập số lượng thiết bị hỏng',
                'SoLuongHong.integer' => 'Số lượng phải là một số nguyên'
            ]
        );

                    $thietbi = new ThietBi;
                    $thietbi->TenThietBi = $request->TenThietBi;
                    $thietbi->TongSoLuong = $request->TongSoLuong;
                    $thietbi->SoLuongHong = $request->SoLuongHong;
                    $thietbi->save();

        return redirect('thietbi/them')->with('thongbao', 'Đã thêm thành công');
    }

    public function getBoSung()
    {
        $thietbi = ThietBi::all();
        return view('thietbi.bosung',['thietbi'=>$thietbi]);
    }

    public function postBoSung(Request $request)
    {

        $this->validate(
            $request,
            [
                'TongSoLuong' => 'required|integer',
                'SoLuongHong' => 'required|integer'
            ],
            [
                'TongSoLuong.required' => 'Bạn chưa nhập số lượng thiết bị tốt',
                'TongSoLuong.integer' => 'Số lượng phải là một số nguyên',
                'SoLuongHong.required' => 'Bạn chưa nhập số lượng thiết bị hỏng',
                'SoLuongHong.integer' => 'Số lượng phải là một số nguyên'
            ]
        );
        $thietbi = ThietBi::all();
        foreach ($thietbi as $tb)
         {
            if ($tb->TenThietBi == $request->TenThietBi)
            {
                (int)$tb->TongSoLuong = (int)$tb->TongSoLuong + (int)$request->TongSoLuong;
                (int)$tb->SoLuongHong = (int)$tb->SoLuongHong + (int)$request->SoLuongHong;
                $tb->save();
                return redirect('thietbi/bosung')->with('thongbao', 'Đã bổ sung thành công');
            }
        }

    }

    public function getSua($id)
    {
        $thietbi = ThietBi::find($id);
        return view('thietbi.sua', ['thietbi' => $thietbi]);
    }

    public function postSua($id, Request $request)
    {
        $this->validate(
            $request,
            [
                'TenThietBi' => 'required',
                'TongSoLuong' => 'required|integer',
                'SoLuongHong' => 'required|integer'
            ],
            [
                'TenThietBi.required' => 'Bạn chưa nhập tên thiết bị',
                'TongSoLuong.required' => 'Bạn chưa nhập số lượng thiết bị',
                'TongSoLuong.integer' => 'Số lượng phải là một số nguyên',
                'SoLuongHong.required' => 'Bạn chưa nhập số lượng thiết bị hỏng',
                'SoLuongHong.integer' => 'Số lượng phải là một số nguyên'
            ]
        );

        $thietbi = ThietBi::find($id);
        $thietbi->TenThietBi = $request->TenThietBi;
        $thietbi->TongSoLuong = $request->TongSoLuong;
        $thietbi->SoLuongHong = $request->SoLuongHong;
        $thietbi->save();

        return redirect('thietbi/sua/' . $id)->with('thongbao', 'Đã sửa thành công');
    }

    public function getXoa($id)
    {
        $thietbi = ThietBi::find($id);
        $thietbi->delete();
        return redirect('thietbi/danhsach')->with('thongbao', 'Đã xóa thành công');
    }
}
