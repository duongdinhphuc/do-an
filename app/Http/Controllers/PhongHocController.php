<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PhongHoc;
class PhongHocController extends Controller
{
    //
    public function getDanhsach()
    {
        $phonghoc = PhongHoc::all();
        return view('phonghoc.danhsach', ['phonghoc' => $phonghoc]);
    }

    public function getThem()
    {
        return view('phonghoc.them');
    }

    public function postThem(Request $request)
    {
        $this->validate(
            $request,
            [
                'TenPhong' => 'required|unique:phonghoc,TenPhong'
            ],
            [
                'TenPhong.required' => 'Bạn chưa nhập tên phòng học',
                'TenPhong.unique' => 'Tên phòng học đã tồn tại'
            ]
        );

        $phonghoc = new PhongHoc;
        $phonghoc->TenPhong = title_case(trans($request->TenPhong));
        $phonghoc->save();

        return redirect('phonghoc/them')->with('thongbao', 'Đã thêm thành công');
    }

    public function getSua($id)
    {
        $phonghoc = PhongHoc::find($id);
        return view('phonghoc.sua', ['phonghoc' => $phonghoc]);
    }

    public function postSua($id, Request $request)
    {
        $phonghoc = PhongHoc::find($id);
        $this->validate(
            $request,
            [
                'TenPhong' => 'required|unique:phonghoc,TenPhong'
            ],
            [
                'TenPhong.required' => 'Bạn chưa nhập tên phòng học',
                'TenPhong.unique' => 'Tên phòng học đã tồn tại'
            ]
        );
        $phonghoc->TenPhong = title_case(trans($request->TenPhong));
        $phonghoc->save();

        return redirect('phonghoc/sua/' . $id)->with('thongbao', 'Đã sửa thành công');
    }

    public function getXoa($id)
    {
        $phonghoc = PhongHoc::find($id);
        $phonghoc->delete();
        return redirect('phonghoc/danhsach')->with('thongbao', 'Đã xóa thành công');
    }
}
