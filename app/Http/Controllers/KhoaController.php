<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Khoa;
class KhoaController extends Controller
{
    //
    public function getDanhsach()
    {
        $khoa = Khoa::all();
        return view('khoa.danhsach', ['khoa' => $khoa]);
    }

    public function getThem()
    {
        return view('khoa.them');
    }

    public function postThem(Request $request)
    {
        $this->validate(
            $request,
            [
                'TenKhoa' => 'required|unique:khoa,TenKhoa'
            ],
            [
                'TenKhoa.required' => 'Bạn chưa nhập tên khoa',
                'TenKhoa.unique' => 'Tên khoa đã tồn tại'
            ]
        );

        $khoa = new Khoa;
        $khoa->TenKhoa = ucfirst($request->TenKhoa);
        $khoa->save();

        return redirect('khoa/them')->with('thongbao', 'Đã thêm thành công');
    }

    public function getSua($id)
    {
        $khoa = Khoa::find($id);
        return view('khoa.sua', ['khoa' => $khoa]);
    }

    public function postSua($id, Request $request)
    {
        $khoa = Khoa::find($id);
        $this->validate(
            $request,
            [
                'TenKhoa' => 'required|unique:khoa,TenKhoa'
            ],
            [
                'TenKhoa.required' => 'Bạn chưa nhập tên khoa',
                'TenKhoa.unique' => 'Tên khoa đã tồn tại'
            ]
        );
        $khoa->TenKhoa = ucfirst(trans($request->TenKhoa));
        $khoa->save();

        return redirect('khoa/sua/' . $id)->with('thongbao', 'Đã sửa thành công');
    }

    public function getXoa($id)
    {
        $khoa = Khoa::find($id);
        $khoa->delete();
        return redirect('khoa/danhsach')->with('thongbao', 'Đã xóa thành công');
    }
}
