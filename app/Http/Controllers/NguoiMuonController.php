<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NguoiMuon;
use App\Khoa;
class NguoiMuonController extends Controller
{
    //
    public function getDanhsach()
    {
        $nguoimuon = NguoiMuon::all();
        return view('nguoimuon.danhsach', ['nguoimuon' => $nguoimuon]);
    }

    public function getThem()
    {
        $nguoimuon = NguoiMuon::all();
        return view('nguoimuon.them', ['nguoimuon' => $nguoimuon]);
    }

    public function postThem(Request $request)
    {
        $this->validate(
            $request,
            [
                'MaNguoiMuon' => 'required|unique:nguoimuon,MaNguoiMuon',
                'TenNguoiMuon' => 'required',
                'Sdt' => 'required'
            ],
            [
                'MaNguoiMuon.required' => 'Bạn chưa nhập mã người mượn',
                'MaNguoiMuon.unique' => 'Người mượn đã tồn tại, vui lòng chuyển qua trang danh sách người mượn !',
                'TenNguoiMuon.required' => 'Bạn chưa nhập tên người mượn',
                'Sdt.required' => 'Bạn chưa nhập số điện thoại'
            ]
        );

        $nguoimuon = new NguoiMuon;
        $nguoimuon->MaNguoiMuon = $request->MaNguoiMuon;
        $nguoimuon->TenNguoiMuon = title_case(trans($request->TenNguoiMuon));
        $nguoimuon->LoaiNguoiMuon = $request->LoaiNguoiMuon;
        $nguoimuon->Sdt = $request->Sdt;
        $nguoimuon->idKhoa = $request->idKhoa;
        $nguoimuon->save();

        return redirect('nguoimuon/them')->with('thongbao', 'Đã thêm thành công');
    }

    public function getSua($id)
    {
        $khoa = Khoa::all();
        $nguoimuon = NguoiMuon::find($id);
        return view('nguoimuon.sua', ['nguoimuon' => $nguoimuon, 'khoa' => $khoa]);
    }

    public function postSua($id, Request $request)
    {
        $this->validate(
            $request,
            [
                'MaNguoiMuon' => 'required',
                'TenNguoiMuon' => 'required',
                'Sdt' => 'required'
            ],
            [
                'MaNguoiMuon.required' => 'Bạn chưa nhập mã người mượn',
                'TenNguoiMuon.required' => 'Bạn chưa nhập tên người mượn',
                'Sdt.required' => 'Bạn chưa nhập số điện thoại'
            ]
        );

        $nguoimuon = NguoiMuon::find($id);
        $nguoimuon->MaNguoiMuon = $request->MaNguoiMuon;
        $nguoimuon->TenNguoiMuon = title_case(trans($request->TenNguoiMuon));
        $nguoimuon->LoaiNguoiMuon = $request->LoaiNguoiMuon;
        $nguoimuon->Sdt = $request->Sdt;
        $nguoimuon->idKhoa = $request->idKhoa;
        $nguoimuon->save();

        return redirect('nguoimuon/sua/' . $id)->with('thongbao', 'Đã sửa thành công');
    }

    public function getXoa($id)
    {
        $nguoimuon = NguoiMuon::find($id);
        $nguoimuon->delete();

        return redirect('nguoimuon/danhsach')->with('thongbao', 'Đã xóa thành công');
    }
}
