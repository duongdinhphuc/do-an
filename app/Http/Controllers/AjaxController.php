<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ThietBi;

class AjaxController extends Controller
{
    //
    public function getDate($stripdate, $tinhtrang)
    {
        $date = MuonTra::whereDate('created_at', '=', $stripdate)->where('TinhTrang', '=', $tinhtrang)->get();
        foreach ($date as $mt)
            echo "<tr class='odd gradeX' align='center'>
                                <td>" . $mt->nguoimuon->TenNguoiMuon . "</td>
                                <td>" . $mt->thietbi->TenThietBi . "</td>
                                <td>" . $mt->SoLuongMuon . "</td>
                                <td>" . date('H:i:s', strtotime($mt->created_at)) . "</td>
                                <td>" . date('H:i:s', strtotime($mt->updated_at)) . "</td>
                                <td>" . $mt->phonghoc->TenPhong . "</td>
                                <td>" . $mt->TinhTrang . "</td>

                                <td class='center><i class='fa fa-pencil fa-fw'></i> <a href='muontra/sua/" . $mt->id . "'>Edit</a></td>
                            </tr>";
    }
}
