<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
class AdminController extends Controller
{
    //
    public function getDangnhap()
    {

        return view('login');
    }
    public function postDangnhap(Request $request)
    {
        $this->validate(
            $request,
            [
                'email' => 'required',
                'password' => 'required|min:3|max:32'
            ],
            [
                'email.required' => 'Bạn chưa nhập email',
                'password.required' => 'Bạn chưa nhập mật khẩu',
                'password.min' => 'Mật khẩu phải có ít nhất 3 ký tự và nhiều nhất là 32 ký tự',
                'password.max' => 'Mật khẩu phải có ít nhất 3 ký tự và nhiều nhất là 32 ký tự'
            ]
        );
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('khoa/danhsach');
        } else {
            return redirect('dangnhap')->with('thongbao', 'Sai tên tài khoản hoặc mật khẩu');
        }
    }
    public function getDangxuat()
    {
        Auth::logout();
        return redirect('dangnhap');
    }
}
