<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThietBi extends Model
{
    //
    protected $table = 'thietbi';

    public function muontra()
    {
        return $this->hasMany('App\MuonTra', 'idThietBi', 'id');
    }
}
