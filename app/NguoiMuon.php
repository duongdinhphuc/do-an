<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NguoiMuon extends Model
{
    //
    protected $table = 'nguoimuon';

    public function khoa()
    {
        return $this->belongsTo('App\Khoa', 'idKhoa', 'id');
    }

    public function muontra()
    {
        return $this->hasMany('App\MuonTra', 'idNguoiMuon', 'id');
    }
}
