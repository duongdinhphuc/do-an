<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MuonTra extends Model
{
    //
    protected $table = 'muontra';





    public function khoa()
    {
        return $this->belongsTo('App\Khoa', 'idKhoa', 'id');
    }

    public function thietbi()
    {
        return $this->belongsTo('App\ThietBi', 'idThietBi', 'id');
    }

    public function nguoimuon()
    {
        return $this->belongsTo('App\NguoiMuon', 'idNguoiMuon', 'id');
    }

    public function phonghoc()
    {
        return $this->belongsTo('App\PhongHoc', 'idPhong', 'id');
    }
}
