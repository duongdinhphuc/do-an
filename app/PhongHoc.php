<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhongHoc extends Model
{
    //
    protected $table = 'phonghoc';

    public function muontra()
    {
        return $this->hasMany('App\MuonTra', 'idPhong', 'id');
    }
}
