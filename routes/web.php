<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\ThietBi;
Route::get('/', function () {
    return view('login');
});

//Khoa
Route::group(['prefix' => 'khoa'], function () {
    Route::get('danhsach', 'KhoaController@getDanhsach');

    Route::get('them', 'KhoaController@getThem');
    Route::post('them', 'KhoaController@postThem');

    Route::get('sua/{id}', 'KhoaController@getSua');
    Route::post('sua/{id}', 'KhoaController@postSua');

    Route::get('xoa/{id}', 'KhoaController@getXoa');
});
//Phong hoc
Route::group(['prefix' => 'phonghoc'], function () {
    Route::get('danhsach', 'PhongHocController@getDanhsach');

    Route::get('them', 'PhongHocController@getThem');
    Route::post('them', 'PhongHocController@postThem');

    Route::get('sua/{id}', 'PhongHocController@getSua');
    Route::post('sua/{id}', 'PhongHocController@postSua');

    Route::get('xoa/{id}', 'PhongHocController@getXoa');
});
//Thiet bi
Route::group(['prefix' => 'thietbi'], function () {
    Route::get('danhsach', 'ThietBiController@getDanhsach');

    Route::get('them', 'ThietBiController@getThem');
    Route::post('them', 'ThietBiController@postThem');

    Route::get('bosung', 'ThietBiController@getBoSung');
    Route::post('bosung', 'ThietBiController@postBoSung');

    Route::get('sua/{id}', 'ThietBiController@getSua');
    Route::post('sua/{id}', 'ThietBiController@postSua');

    Route::get('xoa/{id}', 'ThietBiController@getXoa');
});

//Nguoi muon
Route::group(['prefix' => 'nguoimuon'], function () {
    Route::get('danhsach', 'NguoiMuonController@getDanhsach');

    Route::get('them', 'NguoiMuonController@getThem');
    Route::post('them', 'NguoiMuonController@postThem');

    Route::get('sua/{id}', 'NguoiMuonController@getSua');
    Route::post('sua/{id}', 'NguoiMuonController@postSua');

    Route::get('xoa/{id}', 'NguoiMuonController@getXoa');
});
//Thong tin muon tra
Route::group(['prefix' => 'muontra'], function () {
    Route::get('dsnguoimuon','MuonTraController@getDsNguoiMuon');
    Route::get('danhsach/{idNguoiMuon}', 'MuonTraController@getDanhsach');

    Route::get('thongke','MuonTraController@getThongKe');

    Route::get('them/{id}', 'MuonTraController@getThem');
    Route::post('them/{id}', 'MuonTraController@postThem');

    Route::get('sua/{id}', 'MuonTraController@getSua');
    Route::post('sua/{id}', 'MuonTraController@postSua');

});

//dang nhap admin
Route::get('dangnhap', 'AdminController@getDangnhap');
Route::post('dangnhap', 'AdminController@postDangnhap');
//end dang nhap
//dang xuat
Route::get('dangxuat', 'AdminController@getDangxuat');
//end dang xuat

//loc
Route::get('ajax/{stripdate}&{tinhtrang}', 'AjaxController@getDate');


