@extends('layout.index')
@section('content')
	<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Người mượn
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div>
                            <!---Hiển thị ra lỗi-->
                        @if(count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{ $err }}<br>
                                @endforeach
                            </div>

                        @endif
                        <!--Hiển thị ra thông báo-->
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{ session('thongbao') }}
                            </div>
                        @endif
                        </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Mã người mượn</th>
                                <th>Tên người mượn</th>
                                <th>Loại người mượn</th>
                                <th>Số điện thoại</th>
                                <th>Khoa</th>
                                <th>Delete</th>
                                <th>Edit</th>
                                <th>Go to</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($nguoimuon as $nm)
                            <tr class="odd gradeX" align="center">
                                <td>{{ $nm->MaNguoiMuon }}</td>
                                <td>{{ $nm->TenNguoiMuon }}</td>
                                <td>{{ $nm->LoaiNguoiMuon }}</td>
                                <td>{{ $nm->Sdt }}</td>
                                <td>{{ $nm->khoa->TenKhoa }}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="nguoimuon/xoa/{{ $nm->id }}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="nguoimuon/sua/{{ $nm->id }}">Edit</a></td>
                                <td class="center"><i class="fa fa-book fa-fw"></i> <a href="muontra/them/{{ $nm->id }}">Go to</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection
