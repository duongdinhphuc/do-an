@extends('layout.index')
@section('content')
	<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Người mượn
                            <small>Thêm</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    	<div>
                    		<!---Hiển thị ra lỗi-->
                        @if(count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{ $err }}<br>
                                @endforeach
                            </div>

                        @endif
                        <!--Hiển thị ra thông báo-->
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{ session('thongbao') }}
                            </div>
                        @endif
                    	</div>
                        <form action="nguoimuon/them" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label>Mã người mượn</label>
                                <input class="form-control" name="MaNguoiMuon" placeholder="Nhập mã người mượn" />
                            </div>
                            <div class="form-group">
                                <label>Tên người mượn</label>
                                <input class="form-control" name="TenNguoiMuon" placeholder="Nhập tên người mượn" />
                            </div>


                            <div class="form-group">
                                <label>Loại người mượn</label>
                                <label class="radio-inline">
                                    <input name="LoaiNguoiMuon" value="Giáo viên" type="radio">Giáo viên
                                </label>
                                <label class="radio-inline">
                                    <input name="LoaiNguoiMuon" value="Sinh viên" checked="" type="radio">Sinh viên
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Số điện thoại</label>
                                <input class="form-control" type="number" name="Sdt" placeholder="Nhập số điện thoại" />
                            </div>
                            <div class="form-group">
                                <label>Khoa</label>
                                <select class="form-control" name="idKhoa">
                                	@foreach($nguoimuon as $nm)
                                    	<option value="{{ $nm->idKhoa }}">{{ $nm->khoa->TenKhoa }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection
