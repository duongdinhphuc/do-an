<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <!-- test -->
            <li>
                <a href="#"><i class="fa fa-users fa-fw"></i> Cập nhật dữ liệu<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <!-- Khoa -->
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Khoa<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="khoa/danhsach">Danh sách</a>
                                </li>
                                <li>
                                    <a href="khoa/them">Thêm</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                            </li>
                        <!-- Thiet bi -->
                            <li>
                                <a href="#"><i class="fa fa-users fa-fw"></i> Thiết bị<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="thietbi/danhsach">Danh sách thiết bị</a>
                                    </li>
                                    <li>
                                        <a href="thietbi/them">Thêm</a>
                                    </li>
                                    <li>
                                        <a href="thietbi/bosung">Bổ sung </a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        <!-- Phong hoc -->
                            <li>
                <a href="#"><i class="fa fa-users fa-fw"></i> Phòng học<span class="fa arrow"></span></a>
                <ul class="nav nav-third-level">
                    <li>
                        <a href="phonghoc/danhsach">Danh sách phòng học</a>
                    </li>
                    <li>
                        <a href="phonghoc/them">Thêm phòng học</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <!-- Nguoi muon -->
                <li>
                <a href="#"><i class="fa fa-users fa-fw"></i> Người mượn<span class="fa arrow"></span></a>
                <ul class="nav nav-third-level">
                    <li>
                        <a href="nguoimuon/danhsach">Danh sách người mượn</a>
                    </li>
                    <li>
                        <a href="nguoimuon/them">Thêm người mượn</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <!-- Thong tin muon tra -->
                <li>
                <a href="#"><i class="fa fa-users fa-fw"></i> Thông tin mượn trả<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="muontra/dsnguoimuon">Danh sách những người đang mượn</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
                </ul>
            </li>
            <!-- endtest -->

            <li>
                <a href="#"><i class="fa fa-users fa-fw"></i> Thống kê và tra cứu<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="muontra/thongke">Thống kê mượn trả</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>

        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
