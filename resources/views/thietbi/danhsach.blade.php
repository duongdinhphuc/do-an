@extends('layout.index')
@section('content')
	<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thiết bị
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
					<div>
                        <!---Hiển thị ra lỗi-->
                        @if(count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{ $err }}<br>
                                @endforeach
                            </div>

                        @endif
                        <!--Hiển thị ra thông báo-->
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{ session('thongbao') }}
                            </div>
                        @endif
                    </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Tên thiết bị</th>
                                <th>Tổng số lượng</th>
                                <th>Số lượng hỏng</th>
                                <th>Số lượng tốt</th>
                                <th>Số lượng đang cho mượn</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($thietbi as $tb)
                            <tr class="odd gradeX" align="center">
                                <td>{{ $tb->TenThietBi }}</td>
                                <td>{{ $tb->TongSoLuong }}</td>
                                <td>{{ $tb->SoLuongHong }}</td>
                                <td>{{ (int)$tb->TongSoLuong - (int)$tb->SoLuongHong }}</td>
                                <td>{{ $tb->SoLuongDangMuon }}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="thietbi/xoa/{{ $tb->id }}" }}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="thietbi/sua/{{ $tb->id }}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection
