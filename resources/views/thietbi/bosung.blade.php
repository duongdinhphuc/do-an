@extends('layout.index')
@section('content')
	<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thiết bị
                            <small>Bổ sung</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->

                    <div class="col-lg-7" style="padding-bottom:120px">
                    	<div>
                    		<!---Hiển thị ra lỗi-->
                        @if(count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{ $err }}<br>
                                @endforeach
                            </div>

                        @endif
                        <!--Hiển thị ra thông báo-->
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{ session('thongbao') }}
                            </div>
                        @endif
                    	</div>
                        <form action="thietbi/bosung" method="POST">
                        	<input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label>Tên thiết bị</label>
                                <select class="form-control" name="TenThietBi">
                                	@foreach($thietbi as $tb)
                                    	<option value="{{ $tb->TenThietBi }}">{{ $tb->TenThietBi }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tổng số lượng</label>
                                <input class="form-control" type="number" name="TongSoLuong" placeholder="Nhập số lượng" min="0" />
                            </div>
                            <div class="form-group">
                                <label>Số lượng hỏng</label>
                                <input class="form-control" type="number" name="SoLuongHong" placeholder="Nhập số lượng" min="0" value="0" />
                            </div>
                            <button type="submit" class="btn btn-default">Bổ sung</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection
