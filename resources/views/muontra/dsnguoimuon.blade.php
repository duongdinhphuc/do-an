@extends('layout.index')
@section('content')
	<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh sách những người đã mượn
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div>
                            <!---Hiển thị ra lỗi-->
                        @if(count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{ $err }}<br>
                                @endforeach
                            </div>

                        @endif
                        <!--Hiển thị ra thông báo-->
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{ session('thongbao') }}
                            </div>
                        @endif
                        </div>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">

                        <thead>
                            <tr align="center">
                                <th>Mã người mượn</th>
                                <th>Tên người mượn</th>
                                <th>Go to</th>
                            </tr>
                        </thead>
                        <tbody id="phuc">
                            @foreach($nguoimuon as $nm)
                            <tr class="odd gradeX" align="center">
                                <td>{{ $nm->MaNguoiMuon }}</td>
                                <td>{{ $nm->TenNguoiMuon }}</td>
                                <td class="center"><i class="fa fa-book fa-fw"></i> <a href="muontra/danhsach/{{ $nm->id}}">Go to</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection

