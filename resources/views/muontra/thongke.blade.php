@extends('layout.index')
@section('content')
	<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thống kê
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div>
                            <!---Hiển thị ra lỗi-->
                        @if(count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{ $err }}<br>
                                @endforeach
                            </div>

                        @endif
                        <!--Hiển thị ra thông báo-->
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{ session('thongbao') }}
                            </div>
                        @endif
                        </div>

                        <p>
                            Ngày:
                            <input type="date" id="stripdate">
                        </p>
                        <p>
                            Tình trạng:
                            <select name="tinhtrang" id="tinhtrang">
                                <option value="">Chọn</option>
                                <option value="Chưa trả">Chưa trả</option>
                                <option value="Đã trả">Đã trả</option>
                                <option value="Chưa trả đủ">Chưa trả đủ</option>
                            </select>
                        </p>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">

                        <thead>
                            <tr align="center">
                                <th>Mã NM</th>
                                <th>Tên TB</th>
                                <th>SL mượn</th>
                                <th>SL trả</th>
                                <th>Giờ mượn</th>
                                <th>Giờ trả</th>
                                <th>Phòng học</th>
                                <th>Số tiết mượn</th>
                                <th>Tình trạng</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody id="phuc">
                            @foreach($muontra as $mt)
                            <tr class="odd gradeX" align="center">
                                <td>{{ $mt->nguoimuon->MaNguoiMuon }}</td>
                                <td>{{ $mt->thietbi->TenThietBi }}</td>
                                <td>{{ $mt->SoLuongMuon }}</td>
                                <td>{{ $mt->SoLuongTra}}</td>
                                <td>{{ date('H:i:s', strtotime($mt->created_at)) }}</td>
                                <td><?php if($mt->created_at==$mt->updated_at){
                                    echo null;
                                }
                                else echo date('H:i:s', strtotime($mt->updated_at)) ?></td>
                                <td>{{ $mt->phonghoc->TenPhong }}</td>
                                <td>{{$mt->TietMuon}}</td>
                                <td>{{ $mt->TinhTrang }}</td>

                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="muontra/sua/{{ $mt->id }}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection
@section('script')
    <script>
        $(document).ready(function()
        {
            $('#stripdate, #tinhtrang').change(function()
            {
                var stripdate = $(this).val();
                var tinhtrang = $(this).val();
                //alert(stripdate);
                $.get('ajax/'+stripdate+'&'+tinhtrang, function(data)
                {
                    $('#phuc').html(data);
                });
            });
        });
    </script>
@endsection
