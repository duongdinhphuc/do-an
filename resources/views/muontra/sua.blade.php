@extends('layout.index')
@section('content')
	<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thông tin mượn trả
                            <small>Sửa</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    	<div>
                    		<!---Hiển thị ra lỗi-->
                        @if(count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{ $err }}<br>
                                @endforeach
                            </div>

                        @endif
                        <!--Hiển thị ra thông báo-->
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{ session('thongbao') }}
                            </div>
                        @endif
                    	</div>
                        <form action="muontra/sua/{{ $muontra->id }}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label>Người mượn</label>
                                <input class="form-control" type="text" name="NguoiMuon" placeholder="Nhập số lượng mượn" value="{{ $muontra->nguoimuon->MaNguoiMuon }} - {{ $muontra->nguoimuon->TenNguoiMuon }}" readonly />
                            </div>

                            <div class="form-group">
                                <label>Thiết bị</label>
                                <input class="form-control" type="text" name="ThietBi" placeholder="Nhập số lượng mượn" value="{{ $muontra->idThietBi }} - {{ $muontra->thietbi->TenThietBi }} " readonly  />
                            </div>

                            <div class="form-group">
                                <label>Số lượng mượn</label>
                                <input class="form-control" type="number" name="SoLuongMuon" placeholder="Nhập số lượng mượn" min="1" value="{{ $muontra->SoLuongMuon }}" readonly  />
                            </div>

                            <div class="form-group">
                                <label>Số lượng trả</label>
                                <input class="form-control" type="number" name="SoLuongTra" placeholder="Nhập số lượng trả" min="0" value="{{ $muontra->SoLuongTra }}"  />
                            </div>

                            <div class="form-group">
                                <label>Số lượng hỏng</label>
                                <input class="form-control" type="number" name="SoLuongHong" placeholder="Nhập số lượng hỏng" min="0" value="0"  />
                            </div>

                            <div class="form-group">
                                <label>Phòng học</label>
                                <input class="form-control" type="text" name="PhongHoc" placeholder="Nhập số lượng mượn" value="{{ $muontra->phonghoc->TenPhong}} " readonly  />
                            </div>
                            <div class="form-group">
                                <label>Tiết mượn</label>
                                <input class="form-control" type="text" name="PhongHoc" placeholder="Nhập số lượng mượn" value="{{ $muontra->TietMuon}} " readonly  />
                            </div>
                            <div class="form-group">
                                <label>Tình trạng</label>
                                <label class="radio-inline">
                                    <input name="TinhTrang" value="Đã trả"
                                        @if($muontra->TinhTrang == 'Đã trả' )
                                            {{ "checked" }}
                                        @endif
                                     type="radio">Đã trả
                                </label>
                                <label class="radio-inline">
                                    <input name="TinhTrang" value="Chưa trả"
                                        @if($muontra->TinhTrang == 'Chưa trả' )
                                            {{ "checked" }}
                                        @endif
                                     type="radio">Chưa trả
                                </label>
                                <label class="radio-inline">
                                    <input name="TinhTrang" value="Chưa trả đủ"
                                        @if($muontra->TinhTrang == 'Chưa trả đủ' )
                                            {{ "checked" }}
                                        @endif
                                     type="radio">Chưa trả đủ
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Cập nhật</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection
