@extends('layout.index')
@section('content')
	<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thông tin mượn trả
                            <small>Thêm</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    	<div>
                    		<!---Hiển thị ra lỗi-->
                        @if(count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{ $err }}<br>
                                @endforeach
                            </div>

                        @endif
                        <!--Hiển thị ra thông báo-->
                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                {{ session('thongbao') }}
                            </div>
                        @endif

                    	</div>
                        <form action="muontra/them/{{$nguoimuon->id}}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            {{--  <div class="form-group">
                                <label>Người mượn</label>
                                <select class="form-control" name="NguoiMuon">
                                	@foreach($nguoimuon as $nm)
                                        <option
                                            @if($muontra->idNguoiMuon == $nm->id)
                                                {{ "selected" }}
                                            @endif
                                            value="{{ $nm->id }}"
                                        >{{ $nm->MaNguoiMuon }} - {{ $nm->TenNguoiMuon }}</option>
                                    @endforeach
                                </select>
                            </div>  --}}
                            <div class="form-group">
                                <label>Người mượn</label>
                                <input class="form-control" name="NguoiMuon" placeholder="Nhập người mượn" value="{{$nguoimuon->id}}" readonly/>
                            </div>
                            <div class="form-group">
                                <label>Thông tin về người mượn</label>
                                <input class="form-control" name="TT" placeholder="Nhập người mượn" value="{{ $nguoimuon->MaNguoiMuon }} - {{ $nguoimuon->TenNguoiMuon }}" readonly />
                            </div>
                            <div class="form-group">
                                <label>Thiết bị</label>
                                <select class="form-control" name="ThietBi" id="idThietBi">
                                    @foreach($thietbi as $tb)
                                        <option value="{{ $tb->id }}">{{ $tb->TenThietBi }} - <?php
                                            echo "Còn ";
                                            echo  (int)$tb->TongSoLuong - ((int)$tb->SoLuongDangMuon + (int)$tb->SoLuongHong)." thiết bị";
                                        ?> </option>

                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Số lượng mượn</label>
                                <input class="form-control" type="number" name="SoLuongMuon" placeholder="Nhập số lượng mượn" min="1" />
                            </div>
                            {{--  <div class="form-group">
                                <label>Số lượng trả</label>
                                <input class="form-control" type="number" name="SoLuongMuon" placeholder="Nhập số lượng trả" min="0" />
                            </div>  --}}
                            {{--  <div id="SoLuongCon">

                            </div>  --}}

                            <div class="form-group">
                                <label>Phòng học</label>
                                <select class="form-control" name="PhongHoc">
                                    @foreach($phonghoc as $ph)
                                        <option value="{{ $ph->id }}">{{ $ph->TenPhong }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Số tiết mượn</label>
                                <select class="form-control" name="TietMuon">
                                    <option value="1-3">1 - 3</option>
                                    <option value="4-6">4 - 6</option>
                                    <option value="7-9">7 - 9</option>
                                    <option value="2-5">2 - 5</option>
                                    <option value="1-6">1 - 6</option>
                                    <option value="8-11">8 - 11</option>
                                    <option value="7-12">7 - 12</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tình trạng</label>
                                <label class="radio-inline">
                                    <input name="TinhTrang" value="Đã trả" type="radio">Đã trả
                                </label>
                                <label class="radio-inline">
                                    <input name="TinhTrang" value="Chưa trả" checked="" type="radio">Chưa trả
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Làm mới</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection
@section('script')
        <script>

            $(document).ready(function()
        {
            $('#idThietBi').change(function()
            {

               var sl = $(this).val();

                $.get('ajax/'+sl, function(data)
                {
                    $('#SoLuongCon').html(data);
                });
            });
        });
        </script>
@endsection

