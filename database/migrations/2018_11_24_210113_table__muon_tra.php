<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableMuonTra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('muontra', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idNguoiMuon')->unsigned();
            $table->integer('idThietBi')->unsigned();
            $table->integer('SoLuongMuon');
            $table->integer('SoLuongTra');
            $table->integer('idPhong')->unsigned();
            $table->string('TietMuon');
            $table->string('TinhTrang');
            $table->foreign('idNguoiMuon')->references('id')->on('nguoimuon');
            $table->foreign('idThietBi')->references('id')->on('thietbi');
            $table->foreign('idPhong')->references('id')->on('phonghoc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('muontra');
    }
}
