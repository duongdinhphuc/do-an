<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableNguoiMuon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nguoimuon', function (Blueprint $table) {
            $table->increments('id');
            $table->string('MaNguoiMuon');
            $table->string('TenNguoiMuon');
            $table->string('LoaiNguoiMuon');
            $table->string('Sdt');
            $table->integer('idKhoa')->unsigned();
            $table->foreign('idKhoa')->references('id')->on('khoa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nguoimuon');
    }
}
