<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(KhoaSeeder::class);
        $this->call(NguoiMuonSeeder::class);
        $this->call(ThietBiSeeder::class);
        $this->call(PhongHocSeeder::class);
        $this->call(MuonTraSeeder::class);
    }
}
class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Dương Đình Phúc',
                'email' => 'phuc05121998@gmail.com',
                'password' => bcrypt('phuc'),
                'remember_token' => str_random(10)
            ]
        ]);
    }
}
class KhoaSeeder extends Seeder
{
    public function run()
    {
        DB::table('khoa')->insert([
            [
                'TenKhoa' => 'Công nghệ thông tin'
            ],
            [
                'TenKhoa' => 'Kế toán và quản lý'
            ],
            [
                'TenKhoa' => 'Cơ điện tử'
            ],
            [
                'TenKhoa' => 'Điện tử viễn thông'
            ]
        ]);
    }
}
class NguoiMuonSeeder extends Seeder
{
    public function run()
    {
        DB::table('nguoimuon')->insert([
            [
                'MaNguoiMuon' => 'cd161921',
                'TenNguoiMuon' => 'Dương Đình Phúc',
                'LoaiNguoiMuon' => 'Sinh viên',
                'Sdt' => '0364366032',
                'idKhoa' => '1'
            ],
            [
                'MaNguoiMuon' => 'cd123456',
                'TenNguoiMuon' => 'Bùi Thắng',
                'LoaiNguoiMuon' => 'Sinh viên',
                'Sdt' => '0851452',
                'idKhoa' => '2'
            ],
            [
                'MaNguoiMuon' => 'gv01',
                'TenNguoiMuon' => 'Nguyễn Hoài Linh',
                'LoaiNguoiMuon' => 'Giáo viên',
                'Sdt' => '0364366032',
                'idKhoa' => '3'
            ],
            [
                'MaNguoiMuon' => 'gv02',
                'TenNguoiMuon' => 'Trần Quốc Thư',
                'LoaiNguoiMuon' => 'Giáo viên',
                'Sdt' => '0364366032',
                'idKhoa' => '4'
            ]
        ]);
    }
}
class ThietBiSeeder extends Seeder
{
    public function run()
    {
        DB::table('thietbi')->insert([
            [
                'TenThietBi' => 'Máy chiếu',
                'TongSoLuong' => '50',
                'SoLuongHong' => '0',
                'SoLuongDangMuon' => '0'
            ],
            [
                'TenThietBi' => 'Micro',
                'TongSoLuong' => '50',
                'SoLuongHong' => '0',
                'SoLuongDangMuon' => '0'
            ],
            [
                'TenThietBi' => 'Loa',
                'TongSoLuong' => '50',
                'SoLuongHong' => '0',
                'SoLuongDangMuon' => '0'
            ]
        ]);
    }
}
class PhongHocSeeder extends Seeder
{
    public function run()
    {
        DB::table('phonghoc')->insert([
            [
                'TenPhong' => 'Phòng 401 - A17'
            ],
            [
                'TenPhong' => 'Phòng 402 - A17'
            ],
            [
                'TenPhong' => 'Phòng 403 - A17'
            ],
            [
                'TenPhong' => 'Phòng 404 - A17'
            ],
            [
                'TenPhong' => 'Phòng 405 - A17'
            ],
            [
                'TenPhong' => 'Phòng 501 - A17'
            ]
        ]);
    }
}
class MuonTraSeeder extends Seeder
{
    public function run()
    {
        DB::table('muontra')->insert([
            [
                'idNguoiMuon' => '1',
                'idThietBi' => '2',
                'SoLuongMuon' => '1',
                'SoLuongTra'=>'0',
                'idPhong' => '4',
                'TietMuon' => '1-3',
                'TinhTrang' => 'Chưa trả'
            ],
            [
                'idNguoiMuon' => '2',
                'idThietBi' => '3',
                'SoLuongMuon' => '2',
                'SoLuongTra' => '0',
                'idPhong' => '6',
                'TietMuon' => '4-6',
                'TinhTrang' => 'Chưa trả'
            ],
            [
                'idNguoiMuon' => '3',
                'idThietBi' => '1',
                'SoLuongMuon' => '4',
                'SoLuongTra' => '4',
                'idPhong' => '2',
                'TietMuon' => '7-9',
                'TinhTrang' => 'Đã trả'
            ]
        ]);
    }
}
